# -*- coding: utf8 -*-
# By Pantera Labs

from mongoengine import *
from django.db import models


#connect('taxispeedy')

class Client(models.Model):
    full_name = models.CharField(
        u'Nombres',
        max_length=64
    )
    phone_number = models.CharField(
        u'Telefono',
        max_length=15,
        unique=True
    )
    email = models.EmailField(
        u'Email',
        max_length=100,
        unique=True
    )
    password = models.CharField(
        u'Contraseña',
        max_length=32
    )
    avatar = models.CharField(
        u'Avatar',
        max_length=1
    )
    register_date = models.DateTimeField(
        u'Fecha de registro',
        auto_now_add=True,
        editable=False,
        null=True,
        blank=True
    )

    def __unicode__(self):
        return u"%s" % (self.full_name)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'


class Driver(models.Model):
    first_name = models.CharField(
        u'Nombres',
        max_length=64
    )
    last_name = models.CharField(
        u'Apellidos',
        max_length=64
    )
    plate_number = models.CharField(
        u'Nro Placa',
        max_length=20
    )
    register_date = models.DateTimeField(
        u'Fecha de registro',
        auto_now_add=True,
        editable=False,
        null=True,
        blank=True
    )

    def __unicode__(self):
        return u"%s %s" % (self.first_name, self.last_name)

    class Meta:
        verbose_name = 'Taxista'
        verbose_name_plural = 'Taxistas'


# MongoDB Models

#class DriverPosition(Document):



#class ClientRequest(Document):


