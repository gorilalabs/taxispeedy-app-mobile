# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ws', '0002_auto_20140923_0020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='email',
            field=models.EmailField(unique=True, max_length=100, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.CharField(unique=True, max_length=15, verbose_name='Telefono'),
        ),
    ]
