# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=64, verbose_name='Nombres')),
                ('phone_number', models.CharField(max_length=15, verbose_name='Telefono')),
                ('email', models.CharField(max_length=100, verbose_name='Email')),
                ('password', models.CharField(max_length=32, verbose_name='Contrase\xf1a')),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=64, verbose_name='Nombres')),
                ('last_name', models.CharField(max_length=64, verbose_name='Apellidos')),
                ('plate_number', models.CharField(max_length=20, verbose_name='Nro Placa')),
            ],
            options={
                'verbose_name': 'Taxista',
                'verbose_name_plural': 'Taxistas',
            },
            bases=(models.Model,),
        ),
    ]
