# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ws', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='register_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='driver',
            name='register_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro', null=True),
            preserve_default=True,
        ),
    ]
