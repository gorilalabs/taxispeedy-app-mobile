# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ws', '0003_auto_20140923_0021'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='avatar',
            field=models.CharField(default=None, max_length=1, verbose_name='Avatar'),
            preserve_default=False,
        ),
    ]
