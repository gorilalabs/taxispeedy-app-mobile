# -*- coding: utf8 -*-
#  By Pantera Labs

import json

from forms import ClientForm
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View, TemplateView

class ClientService(View):
    """ ClientService
        
        Register new users from mobile application.
        @returns json
    """

    def post(self, request, *args, **kwargs):
        response = {}
        data = json.loads(request.body)

        client_form = ClientForm(data=data)

        if client_form.is_valid():
            # save user_form
            client = client_form.save()

            response['success'] = True
            response['id'] = client.id
            response['register_date'] = u"%s" % client.register_date
        else:
            response['success'] = False
            response['errors'] = client_form.errors

        return HttpResponse(
                content=json.dumps(response),
                content_type='application/json'
            )