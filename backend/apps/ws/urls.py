# -*- coding: utf8 -*-
# By Pantera Labs

from views import ClientService
from django.conf.urls import patterns, include, url
from django.views.decorators.csrf import csrf_exempt

urlpatterns = patterns('',
    url(r'^client$', 
        csrf_exempt(ClientService.as_view()), 
        name='client_service')
)