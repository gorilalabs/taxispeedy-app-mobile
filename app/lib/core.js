var Alloy = require("alloy");
var io = require('socket.io');
//var HTTP = require("http");

var APP = {
	ID: null,
	ConfigurationURL: null,
	Nodes: [],
	Device: {
		isHandheld: Alloy.isHandheld,
		isTablet: Alloy.isTablet,
		type: Alloy.isHandheld ? "handheld" : "tablet",
		os: null,
		name: null,
		version: Ti.Platform.version,
		versionMajor: parseInt(Ti.Platform.version.split(".")[0], 10),
		versionMinor: parseInt(Ti.Platform.version.split(".")[1], 10),
		width: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformHeight : Ti.Platform.displayCaps.platformWidth,
		height: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformWidth : Ti.Platform.displayCaps.platformHeight,
		dpi: Ti.Platform.displayCaps.dpi,
		orientation: Ti.Gesture.orientation == Ti.UI.LANDSCAPE_LEFT || Ti.Gesture.orientation == Ti.UI.LANDSCAPE_RIGHT ? "LANDSCAPE" : "PORTRAIT",
		statusBarOrientation: null
	},
	Settings: null,
	Network: {
		type: Ti.Network.networkTypeName,
		online: Ti.Network.online
	},
	currentStack: -1,
	previousScreen: null,
	controllerStacks: [],
	modalStack: [],
	hasDetail: false,
	currentDetailStack: -1,
	previousDetailScreen: null,
	detailStacks: [],
	Master: [],
	Detail: [],
	MainWindow: null,
	GlobalWrapper: null,
	ContentWrapper: null,
	ACS: null,
	cancelLoading: false,
	loadingOpen: false,
	SlideMenu: null,
	SlideMenuOpen: false,
	SlideMenuEngaged: true,
	EnabledButtonMenu: true,
	EnabledBackButtonMenu: false,
	EnabledTarifButtonMenu: false,
	beforeSwitch: null,
	longitude: 0,
	latitude: 0,
	latitudeDelta: 0.001,
	longitudeDelta: 0.001,
	address: "",
	avatar: 0,
	backendURL: "http://ws.taxispeedy.com/ws/",
	socketURL: "ws://ws.taxispeedy.com:3000",
	//backendURL: "http://192.168.0.6:8000/ws/",
	//socketURL: "ws://192.168.0.10:3000",
	geocodeURL: "http://maps.google.com/maps/api/geocode/json",
	user: null,
	driverRequest: null,
	_lastLeftTitle: 0,
	BackButtonCallBack: function() {},
	socket: null,
	address: "",
	reference: "",
	/**
	 * Initializes the application
	 */
	init: function() {
		Ti.API.debug("APP.init");

		if(Ti.Platform.osname == "android") {
			//APP.MainWindow.theme = "Theme.NoActionBar";
			//APP.MainWindow.activity.actionBar.hide();
		}

		// Global system Events
		//Ti.Network.addEventListener("change", APP.networkObserver);
		//Ti.Gesture.addEventListener("orientationchange", APP.orientationObserver);
		//Ti.App.addEventListener("pause", APP.exitObserver);
		//Ti.App.addEventListener("close", APP.exitObserver);
		//Ti.App.addEventListener("resumed", APP.resumeObserver);

		//if(OS_ANDROID) {
		//APP.MainWindow.addEventListener("androidback", APP.backButtonObserver);
		//}

		// Determine device characteristics
		//APP.determineDevice();

		// Migrate to newer ChariTi version
		//require("migrate").init();

		// Create a database
		//APP.setupDatabase();

		// Set Customize
		APP.NavigationBar.setBackgroundColor("#be081b");

		// Reads in the JSON config file
		APP.loadContent();

		// Builds out the tab group
		APP.build();

		// Open the main window
		APP.MainWindow.open();

		// The initial screen to show
		//APP.handleNavigation(0);

	},
	connectServer: function() {
		if(APP.socket == null) {
			APP.socket = io.connect(APP.socketURL);

			// Real Time Events
			APP.socket.on('connect', function() {
				Ti.API.log('connected!');
			});

			APP.socket.on('disconnect', function() {
				Ti.API.debug('disconnect :(');
				Ti.API.debug('Trying connecting...');
				try {
					APP.socket.socket.connect();
				} catch(e) {};
			});
		}
		return APP.socket;
	},
	getTarif: function(queryParams, onLoad) {
		var xhr = Titanium.Network.createHTTPClient();
		xhr.onerror = function(e) {
			Ti.API.error('Bad Sever =>' + e.error);
		}

		xhr.open("POST", APP.backendURL + "tarifa");
		xhr.setRequestHeader("content-type", "application/json");

		Ti.API.info('Query Tarif Params ' + JSON.stringify(queryParams));
		xhr.send(JSON.stringify(queryParams));

		xhr.onload = function() {
			Ti.API.info('RAW =' + this.responseText);
			if(this.status == '200') {
				if(this.readyState == 4) {
					var response = JSON.parse(this.responseText);
					onLoad(response);
				} else {
					onLoad(null);
				}
			} else {
				onLoad(null);
				Ti.API.error("Error =>" + this.response);
			}
		};
	},
	registerUser: function(userParams, onLoad, onError) {
		var xhr = Titanium.Network.createHTTPClient();
		xhr.onerror = function(e) {
			Ti.API.error('Bad Sever =>' + e.error);
			onError(e);
		}

		xhr.open("POST", APP.backendURL + "client");
		xhr.setRequestHeader("content-type", "application/json");

		Ti.API.info('User Params ' + JSON.stringify(userParams));
		xhr.send(JSON.stringify(userParams));

		xhr.onload = function() {
			Ti.API.info('RAW =' + this.responseText);
			if(this.status == '200') {
				if(this.readyState == 4) {
					var response = JSON.parse(this.responseText);
					onLoad(response);
				} else {
					onLoad(null);
				}
			} else {
				onLoad(null);
				Ti.API.error("Error =>" + this.response);
			}
		};
	},
	getAddress: function(onLoad) {
		var xhr = Titanium.Network.createHTTPClient();
		xhr.onerror = function(e) {
			Ti.API.error('Bad Sever =>' + e.error);
		}

		xhr.open("GET", APP.geocodeURL + "?latlng=" + APP.latitude + "," + APP.longitude);
		xhr.setRequestHeader("content-type", "application/json");

		Ti.API.info('GEO Params ' + APP.geocodeURL + "?" + APP.latitude + "," + APP.longitude);

		xhr.send();

		xhr.onload = function() {
			Ti.API.info('RAW =' + this.responseText);
			if(this.status == '200') {
				if(this.readyState == 4) {
					var response = JSON.parse(this.responseText);
					onLoad(response);
				} else {
					onLoad(null);
				}
			} else {
				onLoad(null);
				Ti.API.error("Error =>" + this.response);
			}
		};
	},
	existsUser: function() {
		var db = APP.getDatabase();
		var rows = db.execute('SELECT * FROM client LIMIT 1');
		var count = rows.rowCount;
		Ti.API.debug("COUNT ", count);

		while(rows.isValidRow()) {
			APP.user = {
				"full_name": rows.fieldByName("full_name"),
				"phone_number": rows.fieldByName("phone_number"),
				"email": rows.fieldByName("email"),
				"password": rows.fieldByName("password"),
				"avatar": rows.fieldByName("avatar"),
				"register_date": rows.fieldByName("register_date")
			};
			APP.avatar = rows.fieldByName("avatar");
			rows.next();
		}

		rows.close();
		db.close();
		return count;
	},
	saveUser: function(user) {
		var db = APP.getDatabase();
		db.execute('DELETE FROM client');
		db.execute('INSERT INTO client (id, full_name, phone_number, email, password, avatar, register_date) VALUES (?,?,?,?,?,?,?)',
		user.id, user.full_name, user.phone_number, user.email, user.password, user.avatar, user.register_date);
		APP.user = user;
		Ti.API.debug("SAVE ", user.id);
		db.close();
	},
	logout: function() {
		var db = APP.getDatabase();
		db.execute('DELETE FROM client');
		db.close();
	},
	getDatabase: function() {
		return Ti.Database.install('/data/taxispeedy.sqlite', 'taxispeedy');
	},
	loadContent: function() {
		APP.Nodes = [{
			menuHeader: "MENU",
			menuHeaderImage: {
				image: "/images/menu-header.png",
				touchEnabled: false,
				isHeader: true,
				height: 100
			},
			id: 0,
			title: "Perfil",
			controller: "profile",
			image: "/images/profile-icon"
		}, {
			id: 1,
			title: "Salir",
			controller: "logout",
			image: "/images/logout-icon"
		}, {
			id: 2,
			title: "Términos y Condiciones",
			controller: "",
			image: "/images/tyc-icon"
		}];
	},
	build: function() {
		Ti.API.debug("debug", "APP.build");

		var nodes = [];

		for(var i = 0, x = APP.Nodes.length; i < x; i++) {
			nodes.push({
				id: i,
				title: APP.Nodes[i].title,
				image: APP.Nodes[i].image + ".png",
				controller: APP.Nodes[i].controller.toLowerCase(),
				menuHeader: APP.Nodes[i].menuHeader,
				menuHeaderImage: APP.Nodes[i].menuHeaderImage
			});
		}

		if(APP.EnabledButtonMenu) {
			APP.NavigationBar.showLeft({
				image: "/images/menubutton.png",
				callback: function() {
					Ti.API.debug("debug", "Open Menu!");
					if(!APP.SlideMenuOpen) {
						APP.openMenu();
					} else {
						APP.closeMenu();
					}
				}
			});
		} else if(APP.EnabledBackButtonMenu) {
			APP.NavigationBar.showLeft({
				image: "/images/menubackbutton.png",
				callback: function() {
					Ti.API.debug("debug", "Back!!!");
					APP.BackButtonCallBack();
				}
			});
		}

		if(APP.EnabledTarifButtonMenu) {
			APP.NavigationBar.showRight({
				image: "/images/menutarifbutton.png",
				callback: function() {
					APP.openView("tarif");
				}
			});
		}

		APP.buildMenu(nodes);
	},
	buildMenu: function(_nodes) {
		Ti.API.debug("debug", "APP.buildMenu");

		APP.SlideMenu.init({
			nodes: _nodes,
			color: {
				headingBackground: "#3c3c3b",
				headingText: "#a7a9ac"
			}
		});

		// Move everything down to take up the TabGroup space
		APP.ContentWrapper.bottom = "0dp";

		// Add a handler for the nodes (make sure we remove existing ones first)
		APP.SlideMenu.Nodes.removeEventListener("click", APP.handleMenuClick);
		APP.SlideMenu.Nodes.addEventListener("click", APP.handleMenuClick);

		// Listen for gestures on the main window to open/close the slide menu
		APP.GlobalWrapper.addEventListener("swipe", function(_event) {
			if(APP.SlideMenuEngaged) {
				if(_event.direction == "right") {
					APP.openMenu();
				} else if(_event.direction == "left") {
					APP.closeMenu();
				}
			}
		});
	},
	handleMenuClick: function(_event) {
		var controllerName = APP.getControllerNameById(_event.row.id);
		Ti.API.debug("debug", controllerName);
		if(controllerName != null && controllerName != "") {
			if(controllerName == "logout") {
				APP.logout();
				APP.openView("register");
			} else {
				APP.openView(controllerName);
			}
		} else {
			alert(controllerName + " not found!");
		}
	},
	openView: function(controllerName) {
		try {
			if(APP.beforeSwitch != null) APP.beforeSwitch();
			var _view = Alloy.createController(controllerName).getView();
			_view.open();
		} catch(e) {
			Ti.API.error("openView", e);
		}
	},
	getControllerNameById: function(_id) {
		var controllerName = null;
		for(var i = 0, x = APP.Nodes.length; i < x; i++) {
			if(APP.Nodes[i].id == _id) {
				controllerName = APP.Nodes[i].controller.toLowerCase();
				break;
			}
		}
		return controllerName;
	},
	inspectObject: function(obj) {
		var str = "";
		for(var k in _event.row) {
			if(_event.row.hasOwnProperty(k)) {
				str += k + " = " + _event.row[k] + "\n";
			}
		}
		return str;
	},
	/**
	 * Opens the Slide Menu
	 */
	openMenu: function() {
		APP.SlideMenu.Wrapper.setAccessibilityHidden(false);
		APP.NavigationBar.Wrapper.animate({
			left: "250dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP._lastLeftTitle = APP.NavigationBar.titleView.left;

		APP.NavigationBar.titleView.animate({
			left: "250dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.GlobalWrapper.animate({
			left: "250dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.SlideMenu.Wrapper.animate({
			left: "0dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.SlideMenuOpen = true;
	},
	/**
	 * Closes the Slide Menu
	 */
	closeMenu: function() {
		APP.NavigationBar.Wrapper.animate({
			left: "0dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
		APP.NavigationBar.titleView.animate({
			left: APP._lastLeftTitle + "dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.GlobalWrapper.animate({
			left: "0dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.SlideMenu.Wrapper.setAccessibilityHidden(true);
		APP.SlideMenu.Wrapper.animate({
			left: "-250dp",
			duration: 250,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		APP.SlideMenuOpen = false;
	},
	/**
	 * Determines the device characteristics
	 */
	determineDevice: function() {
		if(OS_IOS) {
			APP.Device.os = "IOS";

			if(Ti.Platform.osname.toUpperCase() == "IPHONE") {
				APP.Device.name = "IPHONE";
			} else if(Ti.Platform.osname.toUpperCase() == "IPAD") {
				APP.Device.name = "IPAD";
			}
		} else if(OS_ANDROID) {
			APP.Device.os = "ANDROID";

			APP.Device.name = Ti.Platform.model.toUpperCase();

			// Fix the display values
			APP.Device.width = (APP.Device.width / (APP.Device.dpi / 160));
			APP.Device.height = (APP.Device.height / (APP.Device.dpi / 160));
		}
	}
}

module.exports = APP;