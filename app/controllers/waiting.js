/*
 * Development by PanteraLabs
 */

var APP = require("core");
var Map = require("ti.map");

var map;
var waitGetMyLocation = true;
var waitGetAddress = false;
var timerMove = null;

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

APP.beforeSwitch = function() {
	APP.MainWindow.remove(map);
};

/*// PhoneIcon
var phoneIcon = Ti.UI.createImageView({
	image: "/images/phone-icon.png",
	left: 325,
	top: 400,
	height: 40,
	zIndex: 200
});

phoneIcon.addEventListener("click", function() {
	Titanium.Platform.openURL('tel:965383877');
});

APP.ContentWrapper.add(phoneIcon);*/

// Search
var search = Titanium.UI.createView({
	top: (APP.Device.height <= 480) ? 80 : 60,
	height: 65,
	width: "90%",
	backgroundColor: '#B3dadad9',
	zIndex: 10
});

var qTextFieldBg = Ti.UI.createView({
	backgroundColor: "transparent",
	backgroundImage: "/images/map-textfield-bg.png",
	width: "95%",
	height: 55
});

var qTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "Buscar dirección",
	color: "#c4c0c0",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "90%",
	height: 55,
	left: 5
});

qTextFieldBg.add(qTextField);
search.add(qTextFieldBg);
APP.ContentWrapper.add(search);

// Buttons (My Place | Favorite)
var buttonsContainer = Titanium.UI.createView({
	top: (APP.Device.height <= 480) ? 140 : 130,
	width: Ti.UI.FILL,
	backgroundColor: 'transparent',
	zIndex: 10,
	height: 55,
	layout: "absolute"
});

var myPlaceButton = Ti.UI.createImageView({
	image: "/images/my-place-button.png",
	left: (APP.Device.height <= 480) ? 10 : -290,
	height: 40
});

myPlaceButton.addEventListener("click", function() {
	if(!waitGetMyLocation) waitGetMyLocation = true;
	Titanium.Geolocation.getCurrentPosition(onTrackGPS);
});
buttonsContainer.add(myPlaceButton);

var favoriteButton = Ti.UI.createImageView({
	image: "/images/favorite-button.png",
	left: (APP.Device.height <= 480) ? 260 : Math.abs(myPlaceButton.left),
	right: 0,
	height: 40
});
buttonsContainer.add(favoriteButton);

APP.ContentWrapper.add(buttonsContainer);

// Button Take Taxi
var takeTaxiButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/take-taxi.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

takeTaxiButton.addEventListener("click", function() {
	APP.MainWindow.remove(map);
	APP.address = qTextField.value;
	APP.openView("location");
});

APP.ContentWrapper.add(takeTaxiButton);

// Maps
map = Map.createView({
	id: "Map",
	top: 48,
	width: Ti.UI.FILL,
	mapType: Map.NORMAL_TYPE,
	animate: true,
	regionFit: false,
	userLocation: false,
	height: Ti.UI.FILL,
	enableZoomControls: false,
	zIndex: 5
});

function regionChangedCallback(data) {
	if(APP.longitude == data.longitude) return false;

	if(timerMove != null) clearTimeout(timerMove);

	APP.latitude = data.latitude;
	APP.longitude = data.longitude;

	APP.latitudeDelta = data.latitudeDelta;
	APP.longitudeDelta = data.longitudeDelta;

	Ti.API.debug("debug", APP.longitude + " - " + APP.latitude);

	movePosition();

	timerMove = setTimeout(function() {
		if(!waitGetAddress) {
			qTextField.value = "";
			waitGetAddress = true;
			APP.getAddress(function(response) {
				if(response) {
					qTextField.value = response.results[0].formatted_address;
				}
				waitGetAddress = false;
			});
		}
	}, 5000);
}

map.removeEventListener("regionchanged", regionChangedCallback);
map.addEventListener("regionchanged", regionChangedCallback);

APP.ContentWrapper.add(map);

// Move position on MAP
function movePosition() {
	map.enableZoomControls = false;
	map.setLocation({
		latitude: APP.latitude,
		longitude: APP.longitude,
		latitudeDelta: APP.latitudeDelta,
		longitudeDelta: APP.longitudeDelta
	});

	var me = Map.createAnnotation({
		latitude: APP.latitude,
		longitude: APP.longitude,
		title: "",
		subtitle: '',
		pincolor: Map.ANNOTATION_PURPLE,
		animate: false,
		draggable: false,
		touchEnabled: false,
		image: '/images/pointer.png',
	});

	try {
		map.removeAllAnnotations();
		map.addAnnotation(me);
	} catch(e) {
		Ti.API.error("removeAllAnnotations", e);
	}

	//Ti.API.debug("debug", APP.longitude + " - " + APP.latitude);
}

// GPS
Titanium.Geolocation.ACCURACY_BEST;
Titanium.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;

Titanium.Geolocation.frequency = 5;
Titanium.Geolocation.purpose = "GPS user coordinates";
Titanium.Geolocation.distanceFilter = 10;

function onTrackGPS(e) {
	if(e.error) {
		return;
	}
	APP.longitude = e.coords.longitude;
	APP.latitude = e.coords.latitude;
	altitude = e.coords.altitude;
	heading = e.coords.heading;
	accuracy = e.coords.accuracy;
	speed = e.coords.speed;
	timestamp = e.coords.timestamp;
	altitudeAccuracy = e.coords.altitudeAccuracy;

	movePosition();

	// Enabled
	waitGetMyLocation = false;

	if(!waitGetAddress) {
		waitGetAddress = true;
		qTextField.value = "";
		APP.getAddress(function(response) {
			if(response) {
				qTextField.value = response.results[0].formatted_address;
			}
			waitGetAddress = false;
		});
	}
}

setTimeout(function() {
	Titanium.Geolocation.getCurrentPosition(onTrackGPS);
}, 1000);
//Titanium.Geolocation.removeEventListener('location', onTrackGPSSetter);
//Titanium.Geolocation.addEventListener('location', onTrackGPSSetter);

APP.EnabledButtonMenu = true;
APP.SlideMenuEngaged = true;

APP.init();