/*
 * Development by PanteraLabs
 */

var APP = require("core");
var Map = require("ti.map");

var socket = APP.connectServer();

var map;
var timer;

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

APP.beforeSwitch = function() {
	APP.MainWindow.remove(map);
}

// Callbacks
function skymsgCallback(data) {
	if(data.type == "new-driver") {
		var car = Map.createAnnotation({
			latitude: data.lat,
			longitude: data.long,
			title: "",
			subtitle: '',
			pincolor: Map.ANNOTATION_PURPLE,
			animate: false,
			draggable: false,
			touchEnabled: false,
			image: '/images/taxi-icon.png',
		});
		map.addAnnotation(car);
	}
}

function notifyCallback(data) {
	if(timer != null) clearTimeout(timer);

	APP.driverRequest = data;

	Ti.API.debug(data);

	socket.removeListener('skymsg', skymsgCallback);
	socket.removeListener('notify', notifyCallback);

	APP.MainWindow.remove(map);
	APP.openView("found");
}

var loaderH = 212;

if(APP.Device.height <= 480) loaderH = 100
else if(APP.Device.height <= 600) loaderH = 128

// Loader Animation
var loader = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/loader.png",
	anchorPoint: {
		x: 0.5,
		y: 0.5
	},
	top: loaderH,
	zIndex: 1000,
});

// --> start animation
var matrix2d = Ti.UI.create2DMatrix();
matrix2d = matrix2d.rotate(360); // in degrees

// matrix2d = matrix2d.scale(1.5);
var anim = Ti.UI.createAnimation({
	transform: matrix2d,
	duration: 1000,
	autoreverse: false,
	repeat: 50
});

if(APP.Device.os == "IOS") {
	loader.animate(anim);
} else {
	setTimeout(function() {
		loader.animate(anim);
	}, 1500);
}

APP.ContentWrapper.add(loader);

// Button Cancel Taxi
var cancelTaxiButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/cancel-taxi.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

var cancelTaxiBtn = false;
cancelTaxiButton.addEventListener("click", function() {
	if(cancelTaxiBtn) return false;
	if(timer != null) clearTimeout(timer);

	cancelTaxiBtn = true;
	socket.emit("cancelRequest", {
		"user": APP.user.id,
		"type": "driver-notfound"
	});

	socket.removeListener('skymsg', skymsgCallback);
	socket.removeListener('notify', notifyCallback);

	APP.MainWindow.remove(map);
	APP.openView("waiting");
});

APP.ContentWrapper.add(cancelTaxiButton);

// Maps
var overlay = Ti.UI.createView({
	backgroundColor: "transparent",
	width: Ti.UI.FILL,
	height: Ti.UI.FILL,
	zIndex: 10,
	focusable: true
});

map = Map.createView({
	id: "Map",
	top: 48,
	width: Ti.UI.FILL,
	mapType: Map.NORMAL_TYPE,
	region: {
		latitude: APP.latitude,
		longitude: APP.longitude,
		latitudeDelta: 0.12,
		longitudeDelta: 0.12
	},
	animate: false,
	regionFit: false,
	userLocation: false,
	touchEnabled: false,
	focusable: false,
	height: Ti.UI.FILL,
	enableZoomControls: false,
	zoom: 15,
	zIndex: 5
});

var me = Map.createAnnotation({
	latitude: APP.latitude,
	longitude: APP.longitude,
	title: "",
	subtitle: '',
	pincolor: Map.ANNOTATION_PURPLE,
	animate: false,
	draggable: false,
	touchEnabled: false,
	image: '/images/pointer.png',
});

try {
	map.removeAllAnnotations();
	map.addAnnotation(me);
} catch(e) {
	Ti.API.error("removeAllAnnotations", e);
}

// Socket IO
socket.emit("finddriver", {
	"fullname": APP.user.full_name,
	"long": APP.longitude,
	"lat": APP.latitude,
	"address": APP.address,
	"ref": APP.reference,
	"user": APP.user.id
});

timer = setTimeout(function() {
	socket.emit("cancelRequest", {
		"user": APP.user.id,
		"type": "driver-notfound"
	});

	socket.removeListener('skymsg', skymsgCallback);
	socket.removeListener('notify', notifyCallback);

	APP.MainWindow.remove(map);
	APP.openView("waiting");

}, 40000);

socket.on('notify', notifyCallback);
socket.on('skymsg', skymsgCallback);

APP.ContentWrapper.add(map);
APP.ContentWrapper.add(overlay);

// UI
APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;
APP.EnabledBackButtonMenu = false;

APP.NavigationBar.setTitle("BUSCANDO TAXI");

// Open Window
APP.init();