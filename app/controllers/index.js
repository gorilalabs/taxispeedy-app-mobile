/*
 * Development by PanteraLabs
 */

var APP = require("core");

APP.determineDevice();

Ti.API.debug("Device: " + APP.Device.os + " Height: " + APP.Device.height);

//APP.openView("tarif");

if(APP.existsUser() > 0) {
	APP.openView("waiting");
} else {
	APP.openView("register");
}