/*
 * Development by PanteraLabs
 */

var APP = require("core");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

Ti.API.debug("AVATAR ", APP.avatar);

// Header Of Section
var header = Ti.UI.createImageView({
	top: (APP.Device.height <= 480) ? 80 : 70,
	backgroundColor: "transparent",
	image: "/images/register-header-" + APP.avatar + ".png",
	height: (APP.Device.height <= 480) ? 100 : 115
});
header.addEventListener("click", function() {
	APP.openView("avatar");
});

$.ContentWrapper.add(header);

// Form
var form = Ti.UI.createView({
	top: (APP.Device.height <= 480) ? 180 : 200,
	backgroundImage: "/images/register-form-bg.png",
	width: "90%",
	height: (APP.Device.height <= 480) ? 200 : 250,
	layout: "vertical"
});

var nameTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "NOMBRE",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 40 : 50
});

form.add(nameTextField);

var phoneTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "TELÉFONO",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 70 : 85
});

form.add(phoneTextField);

var emailTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "EMAIL",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 40 : 60
});

form.add(emailTextField);

var passwordTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	passwordMask: true,
	hintText: "CONTRASEÑA",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 70 : 90
});

form.add(passwordTextField);

$.ContentWrapper.add(form);

// Register Button
var registerButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/register-button.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

var waiting = false;
registerButton.addEventListener("click", function() {
	if(waiting) {
		return false;
	}

	waiting = true;
	var userParams = {
		"full_name": nameTextField.value,
		"phone_number": phoneTextField.value,
		"email": emailTextField.value,
		"password": passwordTextField.value,
		"avatar": APP.avatar
	};

	APP.registerUser(userParams, function(response) {
		if(response) {
			if(response.success) {
				var user = userParams;
				user.id = response.id;
				user.register_date = response.register_date;

				APP.saveUser(user);
				APP.openView("waiting");
			} else if(response.errors.email) {
				alert(response.errors.email[0]);
			} else if(response.errors.phone_number) {
				alert(response.errors.phone_number[0]);
			}
		} else {
			alert("Servidor ocupado, intentalo más tarde.");
		}
		waiting = false;
	}, function(error) {
		waiting = false;
	});
});

APP.ContentWrapper.add(registerButton);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;

// Home and Selection Taxi
APP.SlideMenu.setIndex(0);

// Open Window
APP.init();

if(APP.existsUser() > 0) {
	APP.openView("waiting");
}