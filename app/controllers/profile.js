/*
 * Development by PanteraLabs
 */

var APP = require("core");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

Ti.API.debug("AVATAR ", APP.avatar);

// Header Of Section
var header = Ti.UI.createImageView({
	top: (APP.Device.height <= 480) ? 80 : 70,
	backgroundColor: "transparent",
	image: "/images/profile-header-" + APP.avatar + ".png",
	height: (APP.Device.height <= 480) ? 100 : 115
});

$.ContentWrapper.add(header);

// Form
var form = Ti.UI.createView({
	top: (APP.Device.height <= 480) ? 180 : 200,
	backgroundImage: "/images/register-form-bg.png",
	width: "90%",
	height: (APP.Device.height <= 480) ? 200 : 250,
	layout: "vertical"
});

var nameTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "NOMBRE",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 40 : 50,
	editable: false,
	value: APP.user.full_name
});

form.add(nameTextField);

var phoneTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "TELÉFONO",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 70 : 85,
	editable: false,
	value: APP.user.phone_number
});

form.add(phoneTextField);

var emailTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "EMAIL",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 40 : 60,
	editable: false,
	value: APP.user.email
});

form.add(emailTextField);

var passwordTextField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	passwordMask: true,
	hintText: "CONTRASEÑA",
	color: "#FFF",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "80%",
	height: (APP.Device.height <= 480) ? 70 : 90,
	editable: false,
	value: "***********"
});

form.add(passwordTextField);

$.ContentWrapper.add(form);

// Continue Button
var continueButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/continue-button.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

continueButton.addEventListener("click", function() {
	APP.openView("waiting");
});

APP.ContentWrapper.add(continueButton);

APP.EnabledButtonMenu = true;
APP.SlideMenuEngaged = true;

// Home and Selection Taxi
APP.SlideMenu.setIndex(0);

// Open Window
APP.init();

if(APP.existsUser() < 1) {
	APP.openView("register");
}