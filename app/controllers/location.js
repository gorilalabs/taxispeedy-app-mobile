/*
 * Development by PanteraLabs
 */

var APP = require("core");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

// Header Of Section
var header = Ti.UI.createView({
	top: 70,
	backgroundColor: "transparent",
	backgroundImage: "/images/location-form.png",
	width: "90%",
	height: 280,
	layout: "absolute"
});

var fromField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "Dirección",
	color: "#9e9b9b",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "63%",
	height: 50,
	top: 120,
	left: 5,
	value: APP.address
});

header.add(fromField);

var numberField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "Número",
	color: "#9e9b9b",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "32%",
	height: 50,
	top: 120,
	left: '65%'
});

header.add(numberField);

var referenceField = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	hintText: "Ej: afuera del centro comercial",
	color: "#9e9b9b",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "95%",
	height: 50,
	top: 230,
	left: 5
});

header.add(referenceField);

APP.ContentWrapper.add(header);

// Continue Button
var continueButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/continue-button.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

continueButton.addEventListener("click", function() {
	APP.EnabledTarifButtonMenu = false;
	APP.address = fromField.value + " " + numberField.value;
	APP.reference = referenceField.value;
	APP.openView("search");
});

APP.ContentWrapper.add(continueButton);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = true;
APP.EnabledBackButtonMenu = true;
APP.EnabledTarifButtonMenu = true;

APP.BackButtonCallBack = function() {
	APP.EnabledTarifButtonMenu = false;
	APP.openView("waiting");
};

APP.init();