/*
 * Development by PanteraLabs
 */

var APP = require("core");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

var cellWidth = 92;
var cellHeight = 92;
var xSpacer = 2;
var ySpacer = 20;
var xGrid = 3;
var yGrid = 4;

var tableData = [];
var cellIndex = 0;

for(var y = 0; y < yGrid; y++) {
	var thisRow = Ti.UI.createTableViewRow({
		className: "grid",
		touchEnabled: false,
		selectedBackgroundColor: "transparent",
		layout: "horizontal",
		height: cellHeight + 20
	});
	for(var x = 0; x < xGrid; x++) {
		var imageView = Ti.UI.createImageView({
			objName: "grid-view",
			objIndex: cellIndex.toString(),
			image: "/images/icon" + (cellIndex + 1) + ".png",
			backgroundColor: "transparent",
			left: ySpacer,
			height: cellHeight,
			width: cellWidth
		});

		thisRow.add(imageView);
		cellIndex++;
	}
	tableData.push(thisRow);
}
var tableview = Ti.UI.createTableView({
	data: tableData,
	separatorColor: 'transparent',
	selectedBackgroundColor: 'transparent',
	top: 80
});

tableview.addEventListener("click", function(e) {
	if(e.source.objName) {
		APP.avatar = parseInt(e.source.objIndex) + 1;
		Ti.API.info("---> " + e.source.objIndex + " was clicked!");
		APP.openView("register");
	}
});

APP.MainWindow.add(tableview);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;

// Open Window
APP.init();