/*
 * Development by PanteraLabs
 */

var APP = require("core");
var Map = require("ti.map");

var socket = APP.connectServer();

var map;
var alertCount = 0;

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

function moveObjectOnMap() {
	var driver = Map.createAnnotation({
		latitude: APP.driverRequest.driverlat,
		longitude: APP.driverRequest.driverlong,
		title: "",
		subtitle: '',
		pincolor: Map.ANNOTATION_PURPLE,
		animate: false,
		draggable: false,
		touchEnabled: false,
		image: '/images/taxi-icon.png',
	});

	var client = Map.createAnnotation({
		latitude: APP.latitude,
		longitude: APP.longitude,
		title: "",
		subtitle: '',
		pincolor: Map.ANNOTATION_PURPLE,
		animate: false,
		draggable: false,
		touchEnabled: false,
		image: '/images/pointer.png',
	});

	try {
		map.removeAllAnnotations();
		map.addAnnotation(driver);
		map.addAnnotation(client);
	} catch(e) {
		Ti.API.error("removeAllAnnotations", e);
	}
}

// CallBacks
function skymsgCallback(data) {
	if(data.type == "driver-cancel") {
		alert("El taxista cancelo el servicio.");
		setTimeout(function() {
			socket.removeListener('skymsg', skymsgCallback);
			socket.removeListener('onaway', onAwayCallback);

			APP.driverRequest = null;
			APP.MainWindow.remove(map);
			APP.openView("waiting");
		}, 1000);
	} else if(data.type == "onsite") {
		alert("El taxi llego, gracias.");
	}
}

function onAwayCallback(data) {
	if(data) {
		APP.driverRequest.driverlat = data.lat;
		APP.driverRequest.driverlong = data.long;
	}
}

// Search
var infoBox = Titanium.UI.createView({
	top: (APP.Device.height <= 480) ? 80 : 60,
	height: 100,
	width: "90%",
	backgroundColor: '#B3dadad9',
	zIndex: 10
});

var qTextFieldBg = Ti.UI.createView({
	backgroundColor: "transparent",
	backgroundImage: "/images/driver-info-box.png",
	width: "95%",
	height: 80,
	layout: 'absolute'
});

var driverName = Ti.UI.createLabel({
	color: '#575756',
	text: APP.driverRequest.driver_firstname + " " + APP.driverRequest.driver_lastname,
	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
	width: '60%',
	height: 50,
	top: -5,
	left: 100
});

var driverPhone = Ti.UI.createLabel({
	color: '#575756',
	text: "CEL: " + APP.driverRequest.driver_phonenumber,
	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
	width: '60%',
	height: 50,
	top: 12,
	left: 100
});

var driverCar = Ti.UI.createLabel({
	color: '#575756',
	text: "AUTO: " + APP.driverRequest.driver_auto,
	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
	width: '60%',
	height: 50,
	top: 26,
	left: 100
});

var driverPlate = Ti.UI.createLabel({
	color: '#575756',
	text: "PLACA: " + APP.driverRequest.driver_platenumber,
	textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
	width: '60%',
	height: 50,
	top: 40,
	left: 100
});

qTextFieldBg.add(driverName);
qTextFieldBg.add(driverPhone);
qTextFieldBg.add(driverCar);
qTextFieldBg.add(driverPlate);

Ti.API.debug("Imagen: " + APP.driverRequest.driver_imageprofile);

var driverProfile = Titanium.UI.createImageView({
	image: APP.driverRequest.driver_imageprofile,
	width: 80,
	height: 60,
	top: 10,
	left: 10
});

/*function imageIsLoaded() {
	driverProfile.image = APP.driverRequest.driver_imageprofile;
}

driverProfile.removeEventListener('load', imageIsLoaded);
driverProfile.addEventListener('load', imageIsLoaded);*/

qTextFieldBg.add(driverProfile);

infoBox.add(qTextFieldBg);
APP.ContentWrapper.add(infoBox);

// PhoneIcon
var phoneIcon = Ti.UI.createImageView({
	image: "/images/phone-icon.png",
	left: 325,
	top: 400,
	height: 40,
	zIndex: 200
});
APP.ContentWrapper.add(phoneIcon);

phoneIcon.addEventListener("click", function() {
	Titanium.Platform.openURL('tel:' + APP.driverRequest.driver_phonenumber);
});

// Button Cancel / OnBoard Taxi
var cancelButton = Ti.UI.createImageView({
	top: (APP.Device.height <= 480) ? 405 : 500,
	image: "/images/cancel-small-taxi.png",
	left: -150,
	height: 50,
	zIndex: 400
});

var cancelButtonClick = false;
cancelButton.addEventListener("click", function() {
	if(cancelButtonClick) return false;
	cancelButtonClick = true;

	//APP.driverRequest = null;
	socket.emit("cancelRequest", {
		"user": APP.user.id,
		"type": "other",
		"feedback": 0
	});

	socket.emit("skymsg", {
		sid: APP.driverRequest.dsid,
		type: "user-cancel",
		message: "El cliente cancelo el servicio."
	});

	socket.removeListener('skymsg', skymsgCallback);
	socket.removeListener('onaway', onAwayCallback);

	APP.MainWindow.remove(map);
	APP.openView("cancel");
});

APP.ContentWrapper.add(cancelButton);

var onBoardButton = Ti.UI.createImageView({
	top: (APP.Device.height <= 480) ? 405 : 500,
	image: "/images/onboard-taxi.png",
	left: 180,
	right: 0,
	height: 50,
	zIndex: 400
});

var onBoardButtonClick = false;
onBoardButton.addEventListener("click", function() {
	if(onBoardButtonClick) return false;
	onBoardButtonClick = true;

	socket.emit("skymsg", {
		sid: APP.driverRequest.dsid,
		type: "onboard"
	});

	socket.removeListener('skymsg', skymsgCallback);
	socket.removeListener('onaway', onAwayCallback);

	APP.MainWindow.remove(map);
	APP.openView("thanks");
});

APP.ContentWrapper.add(onBoardButton);

map = Map.createView({
	id: "Map",
	top: 48,
	width: Ti.UI.FILL,
	mapType: Map.NORMAL_TYPE,
	region: {
		latitude: APP.latitude,
		longitude: APP.longitude,
		latitudeDelta: 0.05,
		longitudeDelta: 0.05
	},
	animate: false,
	draggable: true,
	touchEnabled: true,
	userLocation: false,
	focusable: true,
	height: Ti.UI.FILL,
	enableZoomControls: false,
	zoom: 10,
	zIndex: 5
});

moveObjectOnMap();

APP.ContentWrapper.add(map);

// Sky MSG
socket.on("skymsg", skymsgCallback);
socket.on('onaway', onAwayCallback);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;

// Open Window
APP.init();