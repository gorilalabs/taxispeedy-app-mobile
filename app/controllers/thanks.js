/*
 * Development by PanteraLabs
 */

var APP = require("core");
var Map = require("ti.map");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

//var socket = APP.connectServer();

// Header Of Section
var header = Ti.UI.createImageView({
	top: 80,
	backgroundColor: "transparent",
	image: "/images/taxispeedy-thanks-head.png",
	left: -70,
	height: 120
});

APP.ContentWrapper.add(header);

// Stars
var star = Ti.UI.createImageView({
	top: 205,
	backgroundColor: "transparent",
	image: "/images/star-on.png",
	left: -280,
	height: 50
});

var labelStar = Ti.UI.createLabel({
	top: 250,
	height: 40,
	color: '#636466',
	backgroundColor: 'transparent',
	text: "Calificar esto",
	textAlign: 'left',
	left: 20,
	zIndex: 300
});

APP.ContentWrapper.add(star);
APP.ContentWrapper.add(labelStar);

// TextArea
var qTextFieldBg = Ti.UI.createView({
	top: 340,
	backgroundColor: "transparent",
	backgroundImage: "/images/textarea-bg.png",
	width: "90%",
	height: 100,
	layout: 'absolute'
});

var labelTextArea = Ti.UI.createLabel({
	top: 300,
	height: 40,
	color: '#FFF',
	backgroundColor: 'transparent',
	text: "Es importante tu opinión",
	textAlign: 'left',
	left: 35,
	zIndex: 400,
	touchEnabled: false,
	font: {
		fontSize: '16'
	}
});

var textArea = Ti.UI.createTextArea({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
	color: "#a7a9ac",
	textAlign: "left",
	backgroundColor: "transparent",
	width: "85%",
	height: 90,
	top: 10,
	left: 0
});

qTextFieldBg.add(textArea);

APP.ContentWrapper.add(labelTextArea);
APP.ContentWrapper.add(qTextFieldBg);

// Button Cancel / OnBoard Taxi
var cancelButton = Ti.UI.createImageView({
	top: 500,
	image: "/images/thanks-cancel.png",
	left: 180,
	right: 0,
	height: 50,
	zIndex: 400
});

cancelButton.addEventListener("click", function() {
	APP.openView("waiting");
});

APP.ContentWrapper.add(cancelButton);

var sendButton = Ti.UI.createImageView({
	top: 500,
	image: "/images/thanks-send.png",
	left: -180,
	height: 50,
	zIndex: 400
});

sendButton.addEventListener("click", function() {
	APP.openView("waiting");
});

APP.ContentWrapper.add(sendButton);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;

APP.NavigationBar.setTitle("TE GUSTO EL APP");

APP.init();