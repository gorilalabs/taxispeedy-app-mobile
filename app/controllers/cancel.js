/*
 * Development by PanteraLabs
 */

var APP = require("core");
var Map = require("ti.map");

APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.NavigationBar = $.NavigationBar;
APP.SlideMenu = $.SlideMenu;

//var socket = APP.connectServer();

var tableData = [];
var options = ["No llegó el taxista", "No pude esperar al taxi", "Otra razón"];

for(var i = 0; i < options.length; i++) {
	var row = Ti.UI.createTableViewRow({
		objName: "grid-view",
		objIndex: i,
		className: "grid",
		touchEnabled: true,
		selectedBackgroundColor: '#face22',
		height: 50
	});

	var label = Ti.UI.createLabel({
		top: 5,
		height: 40,
		color: '#b2b2b1',
		backgroundColor: 'transparent',
		text: options[i],
		textAlign: 'center',
		touchEnabled: false,
		font: {
			fontSize: '20'
		}
	});

	row.add(label);

	tableData.push(row);
}

var tableview = Ti.UI.createTableView({
	data: tableData,
	separatorColor: '#353535',
	selectedBackgroundColor: '#face22',
	top: 180
});

var lastRow = null;
tableview.addEventListener("click", function(e) {
	Ti.API.info("CLICK ---> " + e.source);
	if(e.source.objName) {
		if(lastRow != null) {
			lastRow.backgroundColor = 'transparent';
			lastRow.children[0].color = '#b2b2b1';
		}

		e.source.backgroundColor = '#face22';
		e.source.children[0].color = '#FFF';

		lastRow = e.source;

		Ti.API.info("---> " + e.source.objIndex + " was clicked!");
		//APP.openView("register");
	}
});

APP.ContentWrapper.add(tableview);

// Button
var feedBackButton = Ti.UI.createImageView({
	backgroundColor: "transparent",
	image: "/images/send-feedback.png",
	height: 60,
	top: (APP.Device.height <= 480) ? 405 : 500,
	zIndex: 1000
});

feedBackButton.addEventListener("click", function() {
	//APP.MainWindow.remove(map);
	//APP.address = qTextField.value;

	APP.openView("waiting");
});

APP.ContentWrapper.add(feedBackButton);

APP.EnabledButtonMenu = false;
APP.SlideMenuEngaged = false;

APP.init();